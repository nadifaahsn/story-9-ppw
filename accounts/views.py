from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages
# Create your views here.

from .forms import CreateUserForm

def indexView(request):
    return render(request, 'registration/home.html')

@login_required
def dashboardView(request):
    return render(request, 'registration/dashboard.html')

def registerView(request):
    if request.user.is_authenticated:
        return redirect('accounts:home')
    else:
        form = CreateUserForm()
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('accounts:login_url')

        return render(request, 'registration/register.html', {'form': form})

def loginView(request):
    if request.user.is_authenticated:
    	return redirect('accounts:dashboard')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username = username, password = password)

            if user is not None:
                login(request, user)
                return redirect('accounts:dashboard')
            else:
                messages.error(request, 'Username OR password is incorrect')

        return render(request, 'registration/login.html')

def logoutView(request):
    logout(request)
    return redirect('accounts:login_url')

