from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'password1', 'password2']
		# widgets = {
        #     'username': forms.TextInput(  
        #         attrs={
        #             'class' : 'form-control',
		# 			'placeholder':'Username',
		# 			'type':'text',
		# 			'required':True
        #         }
        #     ),

		# 	'password1': forms.TextInput(  
        #         attrs={
        #             'class' : 'form-control',
		# 			'placeholder':'Password',
		# 			'type':'text',
		# 			'required':True
        #         }
        #     ),

		# 	'password2': forms.TextInput(  
        #         attrs={
        #             'class' : 'form-control',
		# 			'placeholder':'Password Confirmation',
		# 			'type':'text',
		# 			'required':True
        #         }
        #     ),
        # }