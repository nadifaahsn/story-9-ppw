from .apps import AccountsConfig
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .forms import CreateUserForm

# Create your tests here.
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(AccountsConfig.name, 'accounts')
        self.assertEqual(apps.get_app_config('accounts').name, 'accounts')


class Story9UnitTest(TestCase):
    def test_homepage_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_exists(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_login_url_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_url_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 302)

    def test_forms_login_valid(self):
        form_login = CreateUserForm(data={
            "username": "hahaha",
            "password1": "hihihihi123",
            "password2": "hihihihi123"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = CreateUserForm(data={
            "username": "nadifaahsn",
            "password1": "",
            "password2": ""
        })
        self.assertFalse(form_login.is_valid())

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login = reverse("accounts:login_url")
        self.reg = reverse("accounts:register_url")

    def test_POST_login_valid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': 'nadifaahsn',
                                        'password': "PPWasikbanget"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_login_invalid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': '',
                                        'password ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_POST_reg_valid(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        'password1': '123321hahaha',
                                        'password2': '123321hahaha',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_invalid(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        'password1': '123321hahaha',
                                        'password2': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'registration/register.html')

    
